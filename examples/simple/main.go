package main

import (
	"errors"
	"fmt"
	"log"
	"os"

	"codeberg.org/distek/readline"
)

func main() {
	stdin := int(os.Stdin.Fd())
	stdout := int(os.Stdout.Fd())
	stderr := int(os.Stderr.Fd())

	t, err := readline.NewTerm(stdin, stdout, stderr)

	defer func() {
		err := t.Close()
		if err != nil {
			log.Println(err)
		}
	}()

	if err != nil {
		log.Fatal(err)
	}

	t.Prompt = readline.ColorBlue + "somedoapshell" + readline.ColorGreen + " > " + readline.ColorReset

	for {
		line, err := t.Readline()
		if err != nil {
			if errors.Is(err, readline.EOFError) {
				return
			}

			log.Println(err)
		}

		if line == "" {
			continue
		}

		fmt.Println(line)
	}
}
