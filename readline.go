package readline

import (
	"bufio"
	"fmt"
	"math"
	"os"

	"golang.org/x/term"
)

type Term struct {
	OgStdin         int
	OgState         *term.State
	Scanner         *bufio.Scanner
	EOFPrompt       string
	InterruptPrompt string
	Prompt          string
}

const (
	ColorBlack        = "\033[0;30m"
	ColorRed          = "\033[0;31m"
	ColorGreen        = "\033[0;32m"
	ColorYellow       = "\033[0;33m"
	ColorBlue         = "\033[0;34m"
	ColorMagenta      = "\033[0;35m"
	ColorCyan         = "\033[0;36m"
	ColorWhite        = "\033[0;37m"
	ColorLightBlack   = "\033[1;30m"
	ColorLightRed     = "\033[1;31m"
	ColorLightGreen   = "\033[1;32m"
	ColorLightYellow  = "\033[1;33m"
	ColorLightBlue    = "\033[1;34m"
	ColorLightMagenta = "\033[1;35m"
	ColorLightCyan    = "\033[1;36m"
	ColorLightWhite   = "\033[1;37m"
	ColorReset        = "\033[0m"
)

func NewTerm(stdin, stdout, stderr int) (Term, error) {
	var t Term

	ogState, err := term.MakeRaw(stdin)
	if err != nil {
		return Term{}, err
	}

	t.OgState = ogState
	t.OgStdin = stdin

	t.Scanner = bufio.NewScanner(os.Stdin)
	t.Scanner.Split(bufio.ScanBytes)

	t.Prompt = " > "

	t.EOFPrompt = "exit"
	t.InterruptPrompt = "^C"

	return t, nil
}

func (t *Term) Close() error {
	err := term.Restore(t.OgStdin, t.OgState)
	if err != nil {
		return err
	}

	return nil
}

func btoi(b byte) int {
	return int(b) - 48
}

func compileBytes(bytes []byte) int {
	res := 0

	pos := len(bytes) - 1

	for _, v := range bytes {
		// fmt.Println(btoi(v) + (pos * int(math.Pow10(pos))))
		res = res + (btoi(v) + (pos * int(math.Pow10(pos))))
		pos--
	}

	return res
}

func GetPos() (int, int) {
	r := bufio.NewReader(os.Stdout)
	fmt.Print("\033[6n")

	var x int
	var y int

	var currentBuf []byte

	for {
		b, _ := r.ReadByte()

		switch b {
		case 27, 91:
			continue
		case 59:
			y = compileBytes(currentBuf) - 1
			currentBuf = []byte{}
		case 82:
			x = compileBytes(currentBuf) - 1
			return x, y
		default:
			currentBuf = append(currentBuf, b)
		}
	}
}

var (
	// String is "interrupt"
	InterruptError = fmt.Errorf("interrupt")
	// String is "EOF"
	EOFError = fmt.Errorf("EOF")
)

func cursorRight(n int) {
	fmt.Printf("\033[%dC", n)
}

func cursorLeft(n int) {
	fmt.Printf("\033[%dD", n)
}

func cursorUp(n int) {
	fmt.Printf("\033[%dA", n)
}

func cursorDown(n int) {
	fmt.Printf("\033[%dB", n)
}

func cursorCol(n int) {
	fmt.Printf("\033[%dG", n)
}

// Append types
const (
	lineModInsert = iota
	lineModBackspace
	lineModDelete
	lineModCtrlK
	lineModCtrlU
)

func lineMod(buf []byte, b byte, pos int, appendType int) []byte {
	var ret []byte

	switch appendType {
	case lineModInsert:
		if len(buf) == 0 {
			return []byte{b}
		}
		ret = append(buf[:pos], b)
		ret = append(ret, buf[pos:]...)
	case lineModBackspace:
		ret = append(buf[:pos-1], buf[pos:]...)
	case lineModDelete:
		ret = append(buf[:pos], buf[pos+1:]...)
	case lineModCtrlK:
		ret = buf[:pos]
	case lineModCtrlU:
		ret = buf[pos:]
	}

	return ret
}

func getPromptLength(s string) int {
	inEscape := false

	ret := 0
	for _, v := range s {
		switch v {
		case 27:
			inEscape = true
			continue
		case 109:
			inEscape = false
		default:
			if inEscape {
				continue
			}
			ret++
		}
	}

	return ret + 2
}

func cursorXY(x, y int) {
	fmt.Printf("\033[%d;%dH", y, x)
}

func (t Term) Readline() (string, error) {
	fmt.Printf("\r%s", t.Prompt)

	var buf []byte

	inEscape := false

	promptLength := getPromptLength(t.Prompt)

	ox, _ := GetPos()

	pos := ox - promptLength

	for t.Scanner.Scan() {

		prevLen := len(buf)

		bs := t.Scanner.Bytes()
		if len(bs) == 0 {
			continue
		}

		b := bs[0]

		// Debug keys
		// fmt.Println(buf)

		switch b {
		case 3: // ^C
			fmt.Printf("%s\n\r", t.InterruptPrompt)
			return "", InterruptError
		case 4: // ^D
			fmt.Printf("%s\n\r", t.EOFPrompt)
			return "", EOFError
		case 11: // ^K
			buf = lineMod(buf, b, pos, lineModCtrlK)
		case 12: // ^L
			_, y, _ := term.GetSize(int(os.Stdin.Fd()))
			for i := 0; i < y; i++ {
				fmt.Print("\n")
			}

			cursorXY(0, 0)
		case 13: // <CR>
			fmt.Print("\n\r")
			return string(buf), nil

		case 21: // ^U
			buf = lineMod(buf, b, pos, lineModCtrlU)
			cursorLeft(prevLen - len(buf))
			pos = 0
		case 27: // Escape sequence start
			inEscape = true
			continue
		case 51: // 3
			if inEscape {
				continue
			}

			buf = lineMod(buf, b, pos, lineModInsert)
		case 68: // left
			if inEscape {
				if prevLen > 0 {
					// fmt.Println(pos)
					if pos > 0 {
						cursorLeft(1)
						pos--
					}
				}
				inEscape = false
				continue
			}
		case 66: // down
			if inEscape {
				// cursorDown(1)
				inEscape = false
				continue
			}
		case 65: // up
			if inEscape {
				// cursorUp(1)
				inEscape = false
				continue
			}
		case 67: // right
			if inEscape {
				if prevLen > 0 {
					if pos < prevLen-1 {
						cursorRight(1)
						pos++
					}
				}
				inEscape = false
				continue
			}
		case 91: // [
			if inEscape {
				continue
			}

			buf = lineMod(buf, b, pos, lineModInsert)
		case 126: // delete
			if inEscape {
				if prevLen == 0 || pos == prevLen {
					continue
				}

				buf = lineMod(buf, b, pos, lineModDelete)
				inEscape = false
			}
		case 127: // backspace
			if prevLen == 0 || pos == 0 {
				continue
			}

			buf = lineMod(buf, ' ', pos, lineModBackspace)

			pos--
		default:
			buf = lineMod(buf, b, pos, lineModInsert)

			pos++
		}

		fmt.Printf("\033[2K\r%s%s", t.Prompt, string(buf))

		cursorCol(pos + promptLength)
	}

	return "", nil
}
